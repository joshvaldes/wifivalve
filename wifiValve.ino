




//these tank end should reboot themselves often 








/*
Flash Mode "QIO"
Flash Frequency "40MHz"
Upload Using "Serial"
CPU Frequency "80 MHz"
Flash Size "4M"
Reset Method "nodemcu"
 */



#include <ESP8266WiFi.h> //https://github.com/esp8266/Arduino
#include <ESP8266WiFiMulti.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager
#include <ESP8266mDNS.h> 


ESP8266WiFiMulti WiFiMulti;



String explosionMachineName = "wood"; //set a name unique to each explosion machine before flashing, keep it lower case


const char *ssid     = "router";
const char *password = "password";
#define MAX_SRV_CLIENTS 3
String telnetData[MAX_SRV_CLIENTS];


char hostString[8] = {0};//mDNS

String APprefix = "explosion_machine_"; //what to prefix the access point name with for recognition by other devices
String APssid = APprefix + String(ESP.getChipId());


String controllerHostname[MAX_SRV_CLIENTS];
IPAddress controllerIP[MAX_SRV_CLIENTS];
int controllerPort[MAX_SRV_CLIENTS];

WiFiServer telnetServer(23); //telnet port 23
WiFiClient serverClients[MAX_SRV_CLIENTS];

int bootOff = 0; //which serverClients to throw out for a new one, cycle through them

int defaultExplosion = 500;

//relay/valve control
int r1a = 12;
int r1b = 14;
int r2a = 13;
int r2b = 15;

int signsOfLifePin = 16; // change the state of this every second or so to keep the poofers alive
bool lifeToggle;

boolean vOpen = true; //pass true to valve() to open valve
boolean vClose = false;

String newTelegram; //this is a message in from telent
int frameLength = 2000; //how long between poof trains?
const int totalFrames = 7;
int xplodeFrame[totalFrames];

void sayIamAlive(){
  lifeToggle = !lifeToggle;
  digitalWrite(signsOfLifePin, lifeToggle);
}


//open valve if propane == true 
void valve(boolean propane){ 
  if (!propane){
    digitalWrite(r1a, HIGH);
    digitalWrite(r1b, HIGH);
    digitalWrite(r2a, LOW);
    digitalWrite(r2b, LOW);
  }
  if (propane){
    digitalWrite(r1a, LOW);
    digitalWrite(r1b, LOW);
    digitalWrite(r2a, HIGH);
    digitalWrite(r2b, HIGH);
  }
  
} // end valve()


void broadcastMyIP(){
     //mDNS ////////////////////////////////////////
 //  String nameDrop = "explosion_machine_" + explosionMachineName; //
   sprintf(hostString, explosionMachineName.c_str());         //comes out lower case
   WiFi.hostname(hostString);


 if (!MDNS.begin(hostString)) {
 //   Serial.println("Error setting up MDNS responder!");
  }
//  Serial.println("mDNS responder started");
  MDNS.addService("exp", "tcp", 23); // Announce exp tcp service on port 23


}// end broadcastMyIP()



void setup() {
   pinMode(r1a, OUTPUT);
   pinMode(r1b, OUTPUT);
   pinMode(r2a, OUTPUT);
   pinMode(r2b, OUTPUT);
   valve(vClose);

   pinMode(signsOfLifePin, OUTPUT);  //keep this changing at least every second to keep valve relays alive

   WiFiManager wifiManager;

   serveWIFIconnectPage: 
   //go into access point mode and serve a webpage askng for an ssid and PW
   wifiManager.autoConnect(APssid.c_str());
   delay(5000);
   if (WiFi.status() != WL_CONNECTED) goto serveWIFIconnectPage;
   

   // yay I'm connected
   telnetServer.begin();
   telnetServer.setNoDelay(true);

   

/*
  while (WiFiMulti.run() != WL_CONNECTED) {
    delay(500);
    //wait until connected, indicate with LED?
  }
  */
}// end setup()


void testValve(){
  valve(vOpen);
  delay(500);
  valve(vClose);
  delay(500);
}

void explode(int poofTime){
  sayIamAlive(); //toggle the keep alive pin
  valve(vOpen);
  delay(poofTime);
  valve(vClose);
}

void checkForTelnetClients(){

  
if (telnetServer.hasClient()){
    for(int i = 0; i < MAX_SRV_CLIENTS; i++){

      
      //find free/disconnected spot
      if (!serverClients[i] || !serverClients[i].connected()){
        if(serverClients[i]) serverClients[i].stop();
        serverClients[i] = telnetServer.available();
        continue;
      }

    }//end for
    
    if (bootOff >= MAX_SRV_CLIENTS) bootOff = 0;
    
    serverClients[bootOff].stop();
    serverClients[bootOff] = telnetServer.available();
    bootOff++;
     

  }//end if
}//end checkTelnetClients()


//    poofer_miko_explode_800

void checkTelnetsForData(){
//check clients for data
  for(int i = 0; i < MAX_SRV_CLIENTS; i++){
    while(serverClients[i].available()){
      newTelegram = serverClients[i].readStringUntil('\r');
      serverClients[i].flush();
      if (newTelegram.startsWith("poofer_")){
        if (newTelegram.substring((newTelegram.indexOf("poofer_") + 7), newTelegram.indexOf("_explode")) ==  explosionMachineName) { // 7 is length of poofer_ //if this is miko, then what?
          //this is a message for me!
          String xplodeT = newTelegram.substring(newTelegram.indexOf("explode_") + 8);
          explode(xplodeT.toInt());
        }
      }
//////////////////////////////////next potential message
      if (newTelegram.startsWith("explodeAll")){
       
          explode(defaultExplosion);
      }
///////////////////////////////////////////////////////////////// next message is frames. send all frames, then play
      //framePlay frames are 0-7   // frame_0_poofer_miko_explode_400 //feeds all 7 frames to each poofer, then goes to the next
      if (newTelegram.startsWith("frame_")){

        if (newTelegram.substring((newTelegram.indexOf("poofer_") + 7), newTelegram.indexOf("_explode")) ==  explosionMachineName) { // 7 is length of poofer_ //if this is miko, then what?
          //this is a message for me!
          String fff = newTelegram.substring((newTelegram.indexOf("frame_") + 6), newTelegram.indexOf("_poofer"));
          int f = fff.toInt();
          String xff = newTelegram.substring(newTelegram.indexOf("explode_") + 8);
          xplodeFrame[f] = xff.toInt();
        }//if
      }// end if (newTelegram.startsWith("frame_"
      if (newTelegram.startsWith("play")){
        for (int f = 0; f < 7; f++){
          explode(xplodeFrame[f]); //trigger all frames at once
          delay(frameLength - xplodeFrame[f]); // change to delay(frameLength-timeElapsed)
        }//for
      }//if
      
    }//end while
  }//end for

}// end checkTelnetsForData()



void loop() {


sayIamAlive();
broadcastMyIP();
checkForTelnetClients();//
checkTelnetsForData();//explode here


} // end loop
